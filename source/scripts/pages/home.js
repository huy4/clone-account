module.exports = function (sandbox) {
  const _this = this;
  sandbox.on('header/logo/click', () => {
    _this.toggleSection1TitleItalicStyle();
  });

  _this.render = () => {
  };

  return {
    init: (data = {}) => {
      _this.data = data;

      _this.DOMSelectors = {};
      _this.objects = {
      };
      _this.templates = {};

      _this.render();
    },
    destroy: () => {
    },
  };
};
